using Microsoft.AspNetCore.Mvc;
using TravelAPI.DAL;
using Microsoft.AspNetCore.SignalR;


namespace TravelAPI.Controllers
{
    public class BaseController : Controller
    {
        private readonly DataContext _context;
        public BaseController(DataContext context)
        {
            _context = context;
        }
    }
}