using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using TravelAPI.DAL;
using TravelAPI.DAL.Models;
using TravelAPI.DAL.DTO;
using TravelAPI.Services;
using Swashbuckle.AspNetCore.Annotations;
using Microsoft.AspNetCore.Authorization;
using TravelAPI.Config;

namespace TravelAPI.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class TrajetController : ControllerBase
{
    public readonly DataContext CurrentContext;
    private readonly IMapper _mapper;
    public TrajetController(DataContext context, IMapper mapper)
    {
        CurrentContext = context;
        _mapper = mapper;
    }

    [HttpPost("GetTrajetList")]
    [SwaggerOperation("GetTrajetList")]
    //  [Authorize(Constants.SuperAdminRoleName)]
    public ActionResult<TrajetsResponse> GetTrajetList([FromBody] TrajetRequest request)
    {
        return Ok(TrajetService.GetTrajetList(CurrentContext, _mapper, request));
    }

    [HttpPost("CreateTrajet")]
    [SwaggerOperation("CreateTrajet")]
    public ActionResult<TrajetResponse> CreateTrajet([FromBody] TrajetDto trajet)
    {
        return Ok(TrajetService.CreateTrajet(CurrentContext, _mapper, trajet));
    }

}