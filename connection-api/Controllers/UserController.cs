using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TravelAPI.DAL;
using TravelAPI.DAL.Models;
using TravelAPI.DAL.DTO;
using Microsoft.EntityFrameworkCore;
using TravelAPI.Services;
using System.Security.Claims;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using AutoMapper;

namespace TravelAPI.Controllers;

[Produces("application/json")]
[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    public readonly DataContext CurrentContext;
    private readonly IMapper _mapper;

    public UserController(DataContext context, IMapper mapper)
    {
        CurrentContext = context;
        _mapper = mapper;
    }


    [HttpGet]
    [SwaggerOperation("GetConnectedUser")]
    [Authorize("SuperAdmin")]
    public ActionResult<UserDto> GetUser()
    {
        return Ok(UsersService.GetCurrentUser());
    }

    [HttpPost("register")]
    [SwaggerOperation("Register")]
    public ActionResult<UserResponse> Register([FromBody] UserDto request)
    {
        return Ok(UsersService.Register(CurrentContext, _mapper, request));
    }

    [HttpPost("login")]
    [SwaggerOperation("Login")]
    [SwaggerResponse(200, "LoginResponse", typeof(UserResponse))]
    public ActionResult<UserResponse> Login([FromBody] LoginRequest request)
    {
        return Ok(UsersService.Login(CurrentContext, _mapper, request));
    }

}