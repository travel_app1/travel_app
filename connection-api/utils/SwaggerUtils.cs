using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace TravelAPI.Utils.Swagger
{
    public class SwaggerOperationNameFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            operation.OperationId = context.MethodInfo.DeclaringType.GetCustomAttributes(true)
                .Union(context.MethodInfo.GetCustomAttributes(true))
                .OfType<SwaggerOperationAttribute>()
                .Select(a => a.Summary)
                .FirstOrDefault();
        }

    }
    public class ModifySchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            // var values = schema.Properties.Values;
            if (schema.Items != null)
                schema.Items.ReadOnly = false;
            foreach (var schemaProperty in schema.Properties)
            {
                schemaProperty.Value.ReadOnly = false;
            }

        }
    }
}