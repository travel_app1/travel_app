using System.ComponentModel;

namespace TravelAPI.Config
{
    public enum TransportType
    {
        [Description("Bus")]
        Bus = 0,

        [Description("Train")]
        Train = 1,

        [Description("Avion")]
        Avion = 2,

        [Description("AutoCar")]
        AutoCar = 3,
    }

    public static class Constants
    {
        public const string SuperAdminRoleName = "SuperAdmin";
        public const string AdminRoleName = "Admin";
        public const string EmployeeRoleName = "EmployeeRoleName";
    }
}