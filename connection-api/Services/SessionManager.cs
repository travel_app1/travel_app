using TravelAPI.DAL.DTO;

namespace TravelAPI.Services
{
    public interface ISessionManagerInstance
    {
        public ISessionManager Instance { get; }
    }

    public interface ISessionManager
    {
        public UserDto ConnectedUser { get; set; }
        public bool IsConnected { get; set; }
    }

    public sealed class SessionManager
    {
        private static readonly object padlock = new object();

        private SessionManager() { }

        public static ISessionManager Session
        {
            get
            {
                return Manager?.Instance;
            }
        }

        public static ISessionManagerInstance Manager;

        public static void Init(ISessionManagerInstance manager)
        {
            Manager = manager;
        }
    }
}