using TravelAPI.DAL;
using TravelAPI.DAL.DTO;
using System.Diagnostics;
using TravelAPI.DAL.Models;
using AutoMapper;

namespace TravelAPI.Services
{
    public static class DbContextExtension
    {
        public static async Task CreateInitialData(this DataContext context, IMapper mapper)
        {
            try
            {
                //  await CreateRoles(context);
                await CreateInitialUser(context, mapper);
            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }

        public static async Task CreateInitialUser(DataContext context, IMapper mapper)
        {
            try
            {
                var adminRole = context.RolesReadOnly.Where(x => x.RoleCode == "SuperAdmin").FirstOrDefault();

                if (adminRole == null)
                    throw new Exception("Role undefined");



                List<UserDto> usersDto = new List<UserDto>
                {
                    new UserDto
                    {
                        Email = "contact@travel.com",
                        Password = "admin",
                        UserRoles = new List<UserRoleDto>
                        {
                            new UserRoleDto
                            {
                                RoleID = adminRole.ID,
                            }
                        }
                    }
                };

                List<User> users = new List<User>();

                foreach (UserDto userDto in usersDto)
                {
                    User user = new User();

                    UsersService.CreatePasswordHash(userDto.Password, out byte[] passwordHash, out byte[] passwordSalt);

                    user = mapper.Map<User>(userDto);
                    user.PasswordHash = passwordHash;
                    user.PasswordSalt = passwordSalt;

                    users.Add(user);
                }

                await context.Users.AddRangeAsync(users);
                await context.SaveChangesAsync();

            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }
        public static async Task CreateRoles(DataContext context)
        {
            try
            {

                string[] rolesCodes = new string[]
                {
                    "SuperAdmin",
                    "Admin",
                    "Employee",
                    "Client"
                };

                List<Role> roles = new List<Role>();

                foreach (string roleCode in rolesCodes)
                {
                    roles.Add(new Role
                    {
                        RoleCode = roleCode,
                    });
                }

                await context.Roles.AddRangeAsync(roles);

                await context.SaveChangesAsync();

            }
            catch (System.Exception exception)
            {
                Trace.WriteLine(exception.Message);
            }
        }
    }
}