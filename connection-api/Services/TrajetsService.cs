using TravelAPI.DAL.Models;
using TravelAPI.DAL;
using TravelAPI.DAL.DTO;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace TravelAPI.Services;

public class TrajetService
{
    public static TrajetsResponse GetTrajetList(DataContext context, IMapper mapper, TrajetRequest request)
    {
        TrajetsResponse response = new TrajetsResponse();
        try
        {
            var trajetsResponse = context.Trajets.Include(x => x.Employee).ToList();

            if (request.EmployeeID != null)
            {
                trajetsResponse = trajetsResponse.Where(x => x.EmployeeID == request.EmployeeID).ToList();
            }

            if (trajetsResponse != null)
            {
                //  response.Trajets = trajetsResponse.Select(x => x.GetDto()).ToList();
                response.Trajets = mapper.Map<List<TrajetDto>>(trajetsResponse);
            }

            response.Success = true;
        }
        catch (System.Exception err)
        {
            response.HandleResponse(err.Message);
        }
        return response;
    }

    public static TrajetResponse CreateTrajet(DataContext context, IMapper mapper, TrajetDto trajet)
    {
        TrajetResponse response = new TrajetResponse();
        try
        {
            var createTrajet = context.Trajets.Add(mapper.Map<Trajet>(trajet));
            context.SaveChanges();

            if (createTrajet != null)
            {
                response.Trajet = mapper.Map<TrajetDto>(trajet);
                response.Success = true;
            }
        }
        catch (System.Exception exception)
        {
            response.HandleResponse(exception.Message);
        }
        return response;
    }
}
