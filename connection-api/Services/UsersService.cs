using TravelAPI.DAL.DTO;
using System.Security.Cryptography;
using System.Security.Claims;
using TravelAPI.DAL;
using TravelAPI.DAL.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using TravelAPI.Config;
using Newtonsoft.Json;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;


namespace TravelAPI.Services
{
    public class UsersService
    {
        public static User user = new User();
        private static HttpContext _httpContext => new HttpContextAccessor().HttpContext;

        public static UserResponse Register(DataContext context, IMapper mapper, UserDto request)
        {
            UserResponse response = new UserResponse();
            try
            {
                CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Email = request.Email;
                user.LastName = request.LastName;
                user.FirstName = request.FirstName;

                var createUserResponse = context.Users.Add(user);
                context.SaveChanges();

                response.User = mapper.Map<UserDto>(createUserResponse.Entity);
                response.Success = true;
            }
            catch (Exception err)
            {
                response.HandleResponse(err.Message);
            }
            return response;
        }

        public static UserResponse Login(DataContext context, IMapper mapper, LoginRequest request)
        {
            UserResponse response = new UserResponse();
            try
            {
                var userResponse = context.UsersReadOnly.Where(x => x.Email == request.Email).
                                                         Include(x => x.UserRoles).
                                                         ThenInclude(x => x.Role).
                                                         FirstOrDefault();
                if (userResponse == null)
                {
                    throw new Exception("user not found");
                }
                else
                {
                    if (!VerifyPasswordHash(request.Password, userResponse.PasswordHash, userResponse.PasswordSalt))
                    {
                        throw new Exception("Password not correct");
                    }
                }

                response.User = mapper.Map<UserDto>(userResponse);
                response.User.AccessToken = CreateToken(userResponse);

                response.Success = true;
            }
            catch (Exception err)
            {
                response.HandleResponse(err.Message);
            }
            return response;
        }

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return computedHash.SequenceEqual(passwordHash);
            }

        }

        private static string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim("Email", user.Email),
                new Claim("FullName", user.LastName + " " + user.FirstName),
                new Claim("ID", user.ID.ToString()),
            };

            if (user.UserRoles != null)
            {
                foreach (var userRole in user.UserRoles)
                {
                    claims.Add(new Claim("Role", userRole.Role.RoleCode));
                }
            }

            var tokenSecretKey = AppSettings.TokenSecretKey;

            var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(tokenSecretKey));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(2),
                signingCredentials: credentials
            );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public static UserDto? GetCurrentUser()
        {
            var identity = _httpContext.User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                var userClaims = identity.Claims;

                if (userClaims == null)
                    return null;

                var userDto = new UserDto
                {
                    Email = userClaims.FirstOrDefault(x => x.Type == "Email")?.Value!,
                    FullName = userClaims.FirstOrDefault(x => x.Type == "FullName")?.Value!,
                    ID = Int32.Parse(userClaims.FirstOrDefault(x => x.Type == "ID")?.Value!),
                    UserRoles = new List<UserRoleDto>
                    {
                        new UserRoleDto
                        {
                            Role = new RoleDto
                            {
                                RoleCode = userClaims.FirstOrDefault(x => x.Type == "Role")?.Value,
                            }
                        }
                    },
                };

                return userDto;
            }

            return null;
        }
    }
}