using TravelAPI.DAL;
using System.Diagnostics;
using TravelAPI.DAL.Models;
using AutoMapper;

namespace TravelAPI.Services
{
    public static class DatabaseService
    {
        public static void UpdateDatabaseIfNeeded(DataContext context, IMapper mapper)
        {
            try
            {
                context.CreateInitialData(mapper).Wait();
                Console.WriteLine("Database initiated correctly.");

            }
            catch (System.Exception exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
    }
}