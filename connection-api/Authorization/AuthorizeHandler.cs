using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using TravelAPI.DAL;
using TravelAPI.Config;
using TravelAPI.Services;
using TravelAPI.DAL.DTO;


namespace TravelAPI.Authorization
{
    public class AuthorizeHandler : AuthorizationHandler<RoleAuthorizeRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleAuthorizeRequirement requirement)
        {
            if (UsersService.GetCurrentUser() != null)
            {
                var roles = UsersService.GetCurrentUser().UserRoles.Select(x => x.Role.RoleCode).ToList();
                if (roles != null)
                {
                    if (requirement.Roles.Contains(Constants.AdminRoleName) && roles.Contains(Constants.AdminRoleName))
                    {
                        context.Succeed(requirement);
                    }
                    else if (requirement.Roles.Contains(Constants.EmployeeRoleName) && roles.Contains(Constants.EmployeeRoleName))
                    {
                        context.Succeed(requirement);
                    }
                    else if (requirement.Roles.Contains(Constants.SuperAdminRoleName) && roles.Contains(Constants.SuperAdminRoleName))
                    {
                        context.Succeed(requirement);
                    }
                }
            }

            return Task.CompletedTask;
        }
    }
}