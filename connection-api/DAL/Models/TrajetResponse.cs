using TravelAPI.DAL.DTO;

namespace TravelAPI.DAL.Models
{
    public class TrajetResponse : GenericResponse
    {
        public TrajetDto? Trajet { get; set; }
    }

    public class TrajetsResponse : GenericResponse
    {
        public List<TrajetDto>? Trajets { get; set; }
    }
}