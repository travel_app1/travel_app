using System.ComponentModel.DataAnnotations.Schema;


namespace TravelAPI.DAL.Models
{
    [Table("city")]
    public class City : GenericModel
    {
        public string Label { get; set; } = string.Empty;

    }
}