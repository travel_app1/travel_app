using System.ComponentModel.DataAnnotations.Schema;

namespace TravelAPI.DAL.Models
{
    [Table("role")]
    public class Role : GenericModel
    {
        public string? RoleName { get; set; }
        public string? RoleCode { get; set; }

        public virtual ICollection<UserRole>? UserRoles { get; set; }
    }
}