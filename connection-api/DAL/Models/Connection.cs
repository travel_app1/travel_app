using System.ComponentModel.DataAnnotations.Schema;
using TravelAPI.Config;
using TravelAPI.DAL.DTO;

namespace TravelAPI.DAL.Models
{
    [Table("connection")]
    public class Connection : GenericModel
    {
        public virtual City? DepartCity { get; set; }
        public DateTime DepartureTime { get; set; }
        public DateTime? ArrivalTime { get; set; }
        public int Price { get; set; }
        public TransportType TransportType { get; set; }
        public int EmployeeID { get; set; }

        [ForeignKey("EmployeeID")]
        public virtual User? Employee { get; set; }

    }
}