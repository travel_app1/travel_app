
using System.ComponentModel.DataAnnotations.Schema;
using TravelAPI.DAL.DTO;

namespace TravelAPI.DAL.Models;

[Table("users")]
public class User : GenericModel
{
    public string LastName { get; set; } = string.Empty;
    public string FirstName { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public virtual ICollection<UserRole>? UserRoles { get; set; }
    public virtual ICollection<Connection>? Trajets { get; set; }

    public byte[]? PasswordHash { get; set; }
    public byte[]? PasswordSalt { get; set; }
}
