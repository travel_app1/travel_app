using System.ComponentModel.DataAnnotations.Schema;


namespace TravelAPI.DAL.Models
{
    [Table("user-role")]
    public class UserRole : GenericModel
    {
        public int? UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual User? User { get; set; }


        public int? RoleID { get; set; }
        [ForeignKey("RoleID")]
        public virtual Role? Role { get; set; }
    }
}