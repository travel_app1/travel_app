
namespace TravelAPI.DAL.DTO
{
    public class UserRoleDto
    {
        public int ID { get; set; }
        public int? UserID { get; set; }
        public UserDto? User { get; set; }
        public int? RoleID { get; set; }
        public RoleDto? Role { get; set; }
    }
}