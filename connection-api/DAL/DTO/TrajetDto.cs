using TravelAPI.Config;

namespace TravelAPI.DAL.DTO
{
    public class TrajetDto
    {
        public int ID { get; set; }
        public string DepartCity { get; set; } = string.Empty;
        public string ArrivalCity { get; set; } = string.Empty;
        public DateTime DepartureTime { get; set; }
        public DateTime? ArrivalTime { get; set; }
        public int Price { get; set; }
        public TransportType TransportType { get; set; }
        public int EmployeeID { get; set; }
        public UserDto? Employee { get; set; }
    }
}