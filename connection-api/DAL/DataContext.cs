using Microsoft.EntityFrameworkCore;
using TravelAPI.DAL.Models;
using TravelAPI.Config;

namespace TravelAPI.DAL;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options) { }

    public DbSet<User> Users { get; set; }
    public IQueryable<User> UsersReadOnly { get { return Users.AsNoTracking(); } }

    public DbSet<Connection> connections { get; set; }
    public IQueryable<Connection> connectionsReadOnly { get { return connections.AsNoTracking(); } }

    public DbSet<Role> Roles { get; set; }
    public IQueryable<Role> RolesReadOnly { get { return Roles.AsNoTracking(); } }

    public static ILoggerFactory? MyLoggerFactory;
    protected void ConfigureForMysql(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = AppSettings.MySqlConnectionString;
        optionsBuilder.UseMySql(connectionString, MySqlServerVersion.AutoDetect(connectionString),
        x =>
        {
            x.CommandTimeout(60);
        });
        optionsBuilder.EnableSensitiveDataLogging(true);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        ConfigureForMysql(optionsBuilder);
    }
}