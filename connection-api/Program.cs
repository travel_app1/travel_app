using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.DependencyInjection;
using TravelAPI.DAL;
using System.Text;
using Microsoft.OpenApi.Models;
using TravelAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using TravelAPI;
using Swashbuckle.AspNetCore.Filters;
using TravelAPI.Config;
using TravelAPI.Utils.Swagger;
using AutoMapper;
using TravelAPI.Authorization;
using Microsoft.AspNetCore.Authorization;


var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

ConfigureServices(builder.Services, builder.Configuration, builder.Environment.IsDevelopment());

var app = builder.Build();

Configure(app, builder.Environment);

app.Run();

void ConfigureServices(IServiceCollection services, ConfigurationManager configuration, bool IsDevelopment)
{
    var connectionString = configuration.GetConnectionString("MySqlConnectionString");
    services.AddDbContext<DataContext>(options => options.UseMySql(MySqlServerVersion.AutoDetect(connectionString)));
    services.AddMvc();

    services.AddControllers().AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();
        options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
    });
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    var origins = new List<string> { AppSettings.ClientBaseUrl };
    services.AddCors(options =>
    {
        options.AddPolicy("MyPolicy",
            builder =>
                        {
                            builder.AllowAnyHeader();
                            builder.AllowAnyMethod();
                            builder.AllowCredentials();
                            builder.WithOrigins(origins.ToArray());
                        }
        );
    });

    services.AddAuthorization(options =>
        {
            options.AddPolicy(Constants.AdminRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.AdminRoleName })));

            options.AddPolicy(Constants.EmployeeRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.EmployeeRoleName })));

            options.AddPolicy(Constants.SuperAdminRoleName, policy =>
            policy.Requirements.Add(new RoleAuthorizeRequirement(new string[] { Constants.SuperAdminRoleName })));
        }
    );

    services.AddScoped<IAuthorizationHandler, AuthorizeHandler>();

    services.AddSwaggerGen(options =>
    {
        options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
        {
            Description = "Standard Authorization Bearer",
            In = ParameterLocation.Header,
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey
        });

        options.OperationFilter<SecurityRequirementsOperationFilter>();
    });
    services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
       .AddJwtBearer(options =>
       {

           var tokenSecretKey = AppSettings.TokenSecretKey;

           options.TokenValidationParameters = new TokenValidationParameters
           {
               IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                   .GetBytes(tokenSecretKey)
               ),
               ValidateIssuer = false,
               ValidateAudience = false,
           };
       });
    services.AddDistributedMemoryCache();
    services.AddSingleton<Microsoft.AspNetCore.Http.IHttpContextAccessor, Microsoft.AspNetCore.Http.HttpContextAccessor>();

    services.AddSession(options =>
    {
        options.IdleTimeout = TimeSpan.FromMinutes(30);
        options.Cookie.HttpOnly = true;
        options.Cookie.Name = "Travel-AspNetCoreSession";
    });

    services.AddMemoryCache();

    if (IsDevelopment)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v" + 1, new OpenApiInfo { Title = AppSettings.APITitle, Version = "v" + 1 });
            c.OperationFilter<SwaggerOperationNameFilter>();
            c.SchemaFilter<ModifySchemaFilter>();
            c.EnableAnnotations();
        });

        services.AddSwaggerGenNewtonsoftSupport();
    }
}

void Configure(WebApplication app, IWebHostEnvironment env)
{
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint(AppSettings.SwaggerEndPoint, AppSettings.APITitle);
        });
        if (AppSettings.MustSeedDataBase)
        {
            using (var scope = app.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<DataContext>();
                var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();
                DatabaseService.UpdateDatabaseIfNeeded(dbContext, mapper);
            }
        }


    }

    app.UseCors("MyPolicy");

    var origins = new List<string>{
        AppSettings.ClientBaseUrl
    };

    app.UseSession();
    app.UseStaticFiles();
    app.UseHttpsRedirection();
    app.UseAuthentication();

    app.MapControllers();

    app.UseRouting();

    app.UseAuthorization();

    app.UseEndpoints(endpoint =>
    {
        endpoint.MapControllers();

        if (app.Environment.IsDevelopment())
        {
            endpoint.MapGet("/", context =>
            {
                context.Response.Redirect("/swagger");
                return Task.FromResult(0);
            });
        }
        else
            endpoint.MapGet("/", (context) => context.Response.WriteAsync("API OK"));

    });
}

