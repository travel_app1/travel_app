using AutoMapper;
using TravelAPI.DAL.DTO;
using TravelAPI.DAL.Models;

namespace TravelAPI.Profiles
{
    public class TrajetDtoProfile : Profile
    {
        public TrajetDtoProfile()
        {
            CreateMap<Trajet, TrajetDto>();
            CreateMap<User, UserDto>();

            CreateMap<TrajetDto, Trajet>();
            CreateMap<UserDto, User>();
        }
    }
}