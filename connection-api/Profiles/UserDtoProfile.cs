using AutoMapper;
using TravelAPI.DAL.DTO;
using TravelAPI.DAL.Models;

namespace TravelAPI.Profiles
{
    public class UserDtoProfile : Profile
    {
        public UserDtoProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserRole, UserRoleDto>();
            CreateMap<Role, RoleDto>();
            CreateMap<Trajet, TrajetDto>();

            CreateMap<UserDto, User>();
            CreateMap<UserRoleDto, UserRole>();
            CreateMap<RoleDto, Role>();
            CreateMap<TrajetDto, Trajet>();
        }
    }
}