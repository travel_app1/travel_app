using AutoMapper;
using TravelAPI.DAL.DTO;
using TravelAPI.DAL.Models;

namespace TravelAPI.Profiles
{
    public class RoleDtoProfile : Profile
    {
        public RoleDtoProfile()
        {
            CreateMap<Role, RoleDto>();
            CreateMap<UserRole, UserRoleDto>();

            CreateMap<RoleDto, Role>();
            CreateMap<UserRoleDto, UserRole>();
        }
    }
}