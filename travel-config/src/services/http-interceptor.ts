import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';
import { MatDialog } from "@angular/material/dialog";
@Injectable()
export class CustomInterceptor implements HttpInterceptor {

    constructor(
        public dialog: MatDialog
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log("🚀 ~ intercept ~ req", req)
        const access_token = localStorage.getItem('access_token');

        if (access_token) {
            const cloned = req.clone({
                headers: req.headers.set("Authorization", "Bearer " + access_token),
            });

            return this.handleHttpRequest(cloned, next);
        }
        else {
            return this.handleHttpRequest(req, next);
        }
    }

    private handleHttpRequest(req: HttpRequest<any>, next: HttpHandler) {
        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    if (evt.body && evt.body.success)
                        console.log("🚀 ~ handleHttpRequest ~ evt.body", evt.body)
                    //this.toasterService.success(evt.body.success.message, evt.body.success.title, { positionClass: 'toast-bottom-center' });
                }
            }),
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse) {
                    try {
                        console.log("🚀 ~ catchError ~ err", err)
                        // this.toasterService.error(err.error.message, err.error.title, { positionClass: 'toast-bottom-center' });
                    } catch (e) {
                        console.log("🚀 ~ catchError ~ e", e)
                        //this.toasterService.error('An error occurred', '', { positionClass: 'toast-bottom-center' });
                    }
                    //log error 
                }
                return of(err);
            })
        );
    }


}