export * from './trajet.service';
import { TrajetService } from './trajet.service';
export * from './user.service';
import { UserService } from './user.service';
export const APIS = [TrajetService, UserService];
