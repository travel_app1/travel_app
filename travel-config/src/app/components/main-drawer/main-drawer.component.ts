import { ChangeDetectorRef, Component, Input, OnDestroy, ViewChild, ViewEncapsulation } from "@angular/core";
import { MatDrawer } from "@angular/material/sidenav";
import { MediaMatcher } from '@angular/cdk/layout';


@Component({
    selector: 'app-main-drawer',
    templateUrl: './main-drawer.component.html',
    styleUrls: ['./main-drawer.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MainDrawerComponent implements OnDestroy {
    mobileQuery: MediaQueryList;
    private mobileQueryListener: () => void;

    @Input() title?: string;

    constructor(
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
    ) {

        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this.mobileQueryListener);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this.mobileQueryListener);
    }
}