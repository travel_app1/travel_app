import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../guards/guard.service';
import { RouteList } from './route-list';

const routes: Routes = [
  {
    path: RouteList.LOGIN,
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
    pathMatch: 'full'
  },
  {
    path: RouteList.HOME,
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.TRAJET,
    loadChildren: () => import('./pages/trajet/trajets-list.module').then(m => m.TrajetListModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  },
  {
    path: RouteList.TRAJET + '/:id',
    loadChildren: () => import('./pages/edit-trajet/edit-trajet.module').then(m => m.EditTrajetModule),
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
