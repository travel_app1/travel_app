import { Directive } from "@angular/core";
import { AuthData } from "../../global/auth-data";

@Directive({})
export abstract class BaseComponent {
    public loading: boolean = false;
    public AuthData = AuthData;
}