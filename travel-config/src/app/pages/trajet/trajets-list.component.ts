import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TrajetDto, TrajetService } from '../../../services/api-client.generated';
import { BaseComponent } from '../../base/base.component';

@Component({
  selector: 'app-trajet',
  templateUrl: './trajets-list.component.html',
  styleUrls: ['./trajets-list.component.scss']
})
export class TrajetListComponent extends BaseComponent implements OnInit {

  trajetList: TrajetDto[] = [];

  constructor(
    private trajetService: TrajetService,
    private dialog: MatDialog,
  ) {
    super();
  }

  async ngOnInit() {
    await this.loadTrajet();
  }

  async loadTrajet() {
    this.loading = true;

    const response = await this.trajetService.getTrajetList({ EmployeeID: this.AuthData.currentUser.ID }).toPromise();
    console.log("🚀 ~ loadTrajet ~ response", response)
    this.loading = false;

    if (!response?.Success) {
      return;
    }

    this.trajetList = response.Trajets as TrajetDto[];
  }

}
