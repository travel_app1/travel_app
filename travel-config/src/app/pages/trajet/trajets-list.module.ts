import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { MainDrawerModule } from "../../components/main-drawer/main-drawer.module";
import { MatTableModule } from '@angular/material/table';
import { TrajetListComponent } from "./trajets-list.component";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";


const Routes = [
    {
        path: '',
        component: TrajetListComponent
    }
];

@NgModule({
    declarations: [TrajetListComponent],
    imports: [
        CommonModule,
        MainDrawerModule,
        RouterModule.forChild(Routes),
        MatTableModule,
        MatIconModule,
        MatButtonModule,
    ]
})

export class TrajetListModule { }