import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-edit-trajet',
  templateUrl: './edit-trajet.component.html',
  styleUrls: ['./edit-trajet.component.scss', '../../base/base.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditTrajetComponent implements OnInit {
  mode?: "journalier" | "multi";
  @ViewChild(MatStepper) stepper?: MatStepper;
  constructor() { }

  ngOnInit(): void {
  }

  next() {
    this.stepper?.next();
  }

  previous() {
    this.stepper?.previous();
  }

}
