import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditTrajetComponent } from './edit-trajet.component';
import { RouterModule } from '@angular/router';
import { MainDrawerModule } from '../../components/main-drawer/main-drawer.module';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from "@angular/material/icon";
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


const Routes = [
  {
    path: '',
    component: EditTrajetComponent
  }
];

@NgModule({
  declarations: [
    EditTrajetComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(Routes),
    MainDrawerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatRippleModule,
    MatSlideToggleModule,
  ],
  providers: [
    MatDatepickerModule,
  ]
})
export class EditTrajetModule { }
